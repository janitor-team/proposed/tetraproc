Source: tetraproc
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper-compat (= 12),
 libclthreads-dev (>= 2.4.0),
 libclxclient-dev (>= 3.6.1),
 libfftw3-dev (>= 3.1.2-3.1),
 libjack-dev,
 libpng-dev,
 libsndfile-dev,
 libx11-dev
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/multimedia-team/tetraproc.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/tetraproc
Homepage: https://kokkinizita.linuxaudio.org/linuxaudio/index.html
Rules-Requires-Root: no

Package: tetraproc
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Tetrahedral Microphone Processor for Ambisonic Recording
 TetraProc converts the A-format signals from a tetrahedral Ambisonic
 microphone into B-format signals ready for recording. Main features:
 .
  * A-B conversion using a classic scalar matrix and minimum phase
    filters, or
  * A-B conversion using a 4 by 4 convolution matrix using measured
    or computed impulse responses, or a combination of both.
  * Individual microphone calibration facilities.
  * 24 dB/oct higpass filters.
  * Metering, monitoring and test facilities.
  * Virtual stereo mic for stereo monitoring or recording.
  * Unlimited number of stored configurations.
  * Jack client with graphical user interface.